import {
  ADD_TO_CART,
  MORE_LESS_AMOUNT,
  SHOW_DETAIL_INFOR,
} from "../../constants/shoeConstant";
import { dataShoe } from "../../dataShoe";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_DETAIL_INFOR: {
      return { ...state, detail: action.payload };
    }
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });

      if (index === -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case MORE_LESS_AMOUNT: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.idShoe;
      });

      if (index !== -1) {
        cloneCart[index].number =
          cloneCart[index].number + action.payload.soLuong;
      }
      cloneCart[index].number === 0 && cloneCart.splice(index, 1);

      return { ...state, cart: cloneCart };
    }
    default:
      return state;
  }
};
