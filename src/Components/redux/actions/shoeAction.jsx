import {
  ADD_TO_CART,
  MORE_LESS_AMOUNT,
  SHOW_DETAIL_INFOR,
} from "../../constants/shoeConstant";

export const changeDetailAction = (value) => {
  return {
    type: SHOW_DETAIL_INFOR,
    payload: value,
  };
};

export const addShoeToCartAction = (value) => {
  return {
    type: ADD_TO_CART,
    payload: value,
  };
};

export const changeAmountAction = (id, value) => {
  return {
    type: MORE_LESS_AMOUNT,
    payload: {
      idShoe: id,
      soLuong: value,
    },
  };
};
