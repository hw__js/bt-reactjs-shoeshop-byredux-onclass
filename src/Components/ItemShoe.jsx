import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addShoeToCartAction,
  changeDetailAction,
} from "./redux/actions/shoeAction";

class ItemShoe extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="col-3 p-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-text">${price}</h2>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
              className="btn btn-secondary"
            >
              Detail Infor
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-danger"
            >
              Add to Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangeDetail: (shoe) => {
      dispatch(changeDetailAction(shoe));
    },
    handleAddToCart: (shoe) => {
      dispatch(addShoeToCartAction(shoe));
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
