import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { id, name, shortDescription, price, image } = this.props.detail;
    return (
      <div className="row mt-5 alert-secondary text-left">
        <img className="col-3" src={image} alt="" />
        <div className="col-9 mt-5">
          <p>ID: {id}</p>
          <p>Name: {name}</p>
          <p>Desc: {shortDescription}</p>
          <p>Price: ${price}</p>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);
