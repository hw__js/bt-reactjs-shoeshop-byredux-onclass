import React, { Component } from "react";
import { connect } from "react-redux";
import { changeAmountAction } from "./redux/actions/shoeAction";

class Cart extends Component {
  renderTBody = () => {
    return this.props.gioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} alt="" style={{ width: "50px" }} />
          </td>
          <td>${item.price * item.number}</td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleMoreLessAmount(item.id, -1);
              }}
            >
              -
            </button>
            <span className="mx-2">{item.number}</span>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleMoreLessAmount(item.id, +1);
              }}
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Img</th>
            <th>Price</th>
            <th>Quantity</th>
          </tr>
        </thead>
        <tbody>{this.renderTBody()}</tbody>
      </table>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleMoreLessAmount: (id, soLuong) => {
      dispatch(changeAmountAction(id, soLuong));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Cart);
