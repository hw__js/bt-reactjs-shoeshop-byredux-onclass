import "./App.css";
import ShoeShop_Redux from "./Components/ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      <ShoeShop_Redux />
    </div>
  );
}

export default App;
